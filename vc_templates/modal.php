<?php

$atts = ( shortcode_atts( array(
    'listener_id'     => '',
    'box_title'     => '',
    'content' => $content
), $atts ) );


$box_title   = $atts['box_title'];
$box_content = $atts['content'];
$listener_id = $atts['listener_id'];
$listener_id = str_replace( '#', '', $listener_id );

?>

<div class="modal d-none" id="<?php echo esc_attr( $listener_id ) ?>">
    <div class="modal__content">
        <i class="modal__close fa fa-times"></i>
        <?php if ( ! empty( $box_title ) ) {
            printf( '<h4 class="color-white mb-2 text-transform-uppercase" data-title>%s</h4>', $box_title );
        } ?>
        <?php if ( ! empty( $box_content ) ) {
            printf( '<div class="">%s</div>', wpautop( $box_content ) );
        } ?>
    </div>
</div>


<script>
    (function ($) {
        "use strict";
        $(document).ready(function(){
            let listener_id = '<?php printf( '#%s', $listener_id ); ?>';

            $('[href="'+listener_id +'"]').on('click', function (e) {
                let button = $(this),
                    buttonHref = button.attr('href');

                if (listener_id === buttonHref) {
                    let modal = document.querySelector(buttonHref);

                    if(button.data('title')){

                        $(modal).find('[data-title]').eq(0).html(button.data('title'))
                    }
                    if(button.data('id')){
                        $(modal).find('#input_1_14').eq(0).val(button.data('id'));
                    }
                    if(button.data('email')){
                        $(modal).find('#input_1_15').eq(0).val(button.data('email'));
                    }


                    $(modal).toggleClass('d-flex')
                    $(window).click(function (e) {
                        if ($(e.target).hasClass('modal') || $(e.target).hasClass('modal__close')) {
                            $('.modal').trigger('reset');
                            $('.modal').find('.validation_message').remove();
                            $('.modal').find('.gform_validation_errors').remove();
                            $('.modal').find('input, select').attr('aria-invalid', false);
                            $('.modal').removeClass('d-flex');
                        }
                    });
                }

            });
        })
    })(jQuery);
</script>

