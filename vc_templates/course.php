<?php


$post = get_post($atts['id']);

?>

    <div class="holes">
      <?php
      $holes = get_field('holes', $post->ID);
      foreach ($holes as $key => $hole) {
        ?>
          <div class="wrapper">
              <div class="hole bg-light p-3 p-lg-5 mb-4">
                  <div>
                      <p class="h1 fw-700 color-accent"><?= $key + 1 ?></p>
                      <p class="h5 text-transform-uppercase "><?= __('Par ', 'royalquebec') . $hole['custom_number'] ?></p>
                  </div>
                  <div>
                      <img class="" src="<?= $hole['img_1']['url'] ?>" alt="">
                  </div>
                  <div>
                      <img class="" src="<?= $hole['img_2']['url'] ?>" alt="">
                  </div>
                  <div>
                      <img class="" src="<?= $hole['img_3']['url'] ?>" alt="">
                  </div>
              </div>
            <?php
            ?>
          </div>
        <?php

      }
      ?>
    </div>
<?php


wp_reset_postdata();

