
<?php
    if($atts['custom_image_position']){
        $wrapper_class = 'd-flex flex-row-reverse';
        $div_class = 'ml-auto';
    } else {
        $div_class = 'mr-auto';
    }
?>

<div class="vc_row wpb_row vc_row-fluid section-image-text-full vc_row-o-equal-height vc_row-o-content-middle vc_row-flex <?= $wrapper_class ?>" >
    <div class="wpb_column vc_column_container vc_col-sm-5 vc_col-md-6 ">
        <div class="vc_column-inner px-0">
            <div class="wpb_wrapper">
                <div class="wpb_single_image wpb_content_element vc_align_left mb-0">
                    <figure class="wpb_wrapper vc_figure">
                        <div class="vc_single_image-wrapper vc_box_border_grey">
                            <?php
                                if($atts['custom_image']){
                                    echo wp_get_attachment_image($atts['custom_image'], 'full');
                                }
                            ?>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <div class="p-4  wpb_column vc_column_container vc_col-sm-7 vc_col-md-6 <?= $div_class ?>" >
        <div class="vc_column-inner px-0">
            <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element">
                    <div class="wpb_wrapper">
                        <?php
                        if($atts['custom_title']){
                            echo '<h2 class="h4 text-normal">'.$atts['custom_title'].'</h2>';
                        }
                        ?>
                    </div>
                </div>

                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <?php
                        if($atts['custom_content']){
                            echo '<p>'. $atts['custom_content'] .'</p>';
                        }
                        if ($content) {
//                            $content = wpb_js_remove_wpautop($content, true);

                            echo $content;
                        }
                        ?>
                    </div>
                </div>
                    <?php
                    if($atts['custom_btn']){
                        $link = vc_build_link( $atts['custom_btn'] );

                        if(!empty($link['url'])){
                            echo '<a class="btn btn-accent"';
                            echo $link['url'] ? ' href="' . $link['url'] . '"' : '';
                            echo $link['target'] ? ' target="' . $link['target'] . '"' : '';
                            echo '>';
                            echo $link['title'];
                            echo ' </a>';
                        }

                    }
                    ?>
            </div>
        </div>
    </div>
</div>
