<?php

$atts = ( shortcode_atts( array(
    'btn_shape'     => 'squared',
    'btn_color'     => 'primary',
    'btn_align'    => 'normal',
    'btn_text'    => '',
    'css' => '',
    'extra_class' => '',
    'extra_id' => '',
    'btn_link' => '#'
), $atts ) );

    $wrapper_id = '';
    $class = '';
    $wrapper_class = '';

    $class .= $atts['btn_shape'] == 'rounded' ? 'btn-round ' : 'btn ';
    $class .= 'btn-' . $atts['btn_color'];

    if($atts['btn_align'] !== 'normal'){
        $wrapper_class .= $atts['btn_align'] ? ' text-align-' . $atts['btn_align'] . ' ' : '';
    }

    $link = vc_build_link( $atts['btn_link'] );


    $wrapper_id .= $atts['extra_id'];
    $wrapper_class .= $atts['extra_class'];

    $target = !empty($link['target']) ? 'target="_blank"' : '';
?>

<div class="btn-wrapper <?= $wrapper_class ?>" id="<?= $wrapper_id ?>">
    <style>
        <?php echo $atts['css'] ?>
    </style>
    <a
            <?= $target ?>
            class="<?= $class ?>"
            href="<?= $link['url'] ?>"
    >
        <?= $link['title'] ?>
    </a>
</div>


