<section class="vc_section section-lg fill-bg" style="background-image: url(<?php echo wp_get_attachment_image_url($atts['custom_image'], 'full') ?>);">
    <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element  color-white">
                        <div class="wpb_wrapper">
                            <?php
                            if ($atts['custom_title']) {
                                ?>
                                <h3 class="accent-text" style="text-align: center;">
                                    <em><?= $atts['custom_title'] ?></em>
                                </h3>
                                <?php
                            }
                            ?>
                            <?php
                            if ($atts['custom_subtitle']) {
                                ?>
                                <h4 style="font-weight: 500; text-align: center;"><?= $atts['custom_subtitle'] ?></h4>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


