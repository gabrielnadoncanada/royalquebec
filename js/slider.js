
(function ($) {
    if ($('.hero').length > 0) {
        $('.hero').slick({
            dots: true,
            autoplaySpeed: 3000,
        });
    }

    if ($('.partners .slick').length > 0) {
        $('.partners .slick').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,

            variableWidth: true,
            autoplaySpeed: 3000,
        });
    }

    if ($('.holes').length > 0) {
        $('.holes').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrow: true,
            adaptiveHeight: true,
            easing: 'swing',
            speed: 700,
            infinite: false,
            dots: true,
            customPaging: function (slick, index) {
                return '<a>' + (index + 1) + '</a>';
            }
        });
    }
}(jQuery));
