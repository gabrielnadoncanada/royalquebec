(function ($) {
    $('header .menu-item-has-children > a').each(function(){
        $(this).attr('href', 'javascript:void(0)')
    })

    $('.search_toggle').click(function (e) {
        $('.search_form').toggleClass('toggled');
        e.stopPropagation();
    })

    $(document).click(function (e) {
        if (e.target.hash == '#open-video') {
            if (!$('.video_modal_wrapper').length > 0) {
                render_videos_modal($(e.target));
            } else {
                $('.video_modal_wrapper').remove();
            }
        }

        if (e.target.classList.value == 'video_modal_wrapper') {
            $('.video_modal_wrapper').remove();
        }

        if ($('.vc_tta-tabs-list.overflow-y-visible').length > 0 && !e.target.classList.contains('vc_tta_tabs_button')) {
            $('.vc_tta-tabs-list.overflow-y-visible').removeClass('overflow-y-visible')
        }

        const isClickSearch = $('.search_form').get(0).contains(e.target);
        if (!isClickSearch && $('.search_form').hasClass('toggled')) {
            $('.search_form').toggleClass('toggled');
        }
    })


    $('.menu-wrapper div[class^="menu-primary-navigation"] .menu > .menu-item  > a').on('mouseover', function (e) {
        if ($(window).width() >= 1024) {
            e.preventDefault()
            let items = $('.menu-wrapper div[class^="menu-primary-navigation"] .menu > .menu-item ');

            items.removeClass('active');
            if ($(this).parent().hasClass('menu-item-has-children')) {
                $(this).parent().addClass('active');

            }
        }
    });

    $('.menu-item').on('click', function (e) {
        if ($(window).width() < 1024) {
            $(this).find('ul').toggleClass('active');
        }
    })

    if ($(this).width() >= 1024) {
        $('header .menu-toggle').on('mouseover', function () {
            if (!$(this).parent().hasClass('toggled')) {
                $(this).parent().addClass('toggled');
                $(this).attr('aria-expanded', 'true')
            }
        })
    }

    if ($('.vc_row.section-image-text-full').length) {
        function isodd(x) {
            return (x & 1) ? 1 : 0;
        }

        $('.vc_row.section-image-text-full').each(function (index) {
            let image = $(this).find('.wpb_single_image').get(0);
            let text = $(this).find('.vc_col-sm-7').get(0);

            if (isodd(index)) {
                image.setAttribute('data-aos', 'fade-left');
                image.setAttribute('data-aos-duration', '850');
                text.setAttribute('data-aos', 'fade-right');
                text.setAttribute('data-aos-duration', '850');
                text.setAttribute('data-aos-easing', 'ease-in');

            } else {
                image.setAttribute('data-aos', 'fade-right');
                image.setAttribute('data-aos-duration', '850');
                text.setAttribute('data-aos', 'fade-left');
                text.setAttribute('data-aos-duration', '850');
                text.setAttribute('data-aos-easing', 'ease-in');
            }
        })
    }

    $(function () {
        if ($.fn.vcAccordion) {
            var _isAnimated = $.fn.vcAccordion.Constructor.prototype.isAnimated;
            $.fn.vcAccordion.Constructor.prototype.isAnimated = function () {
                return 0;
            }
        }
    });

    function showPlaceholder() {
        if ($('.placeholder .ginput_container').length > 0) {
            $('.placeholder .ginput_container > *').on({
                change: function (e) {
                    if ($(this).val() != '') {
                        $(this).closest('.placeholder').addClass('hasValue');
                    } else {
                        $(this).closest('.placeholder').removeClass('hasValue');
                    }
                },
                keyup: function (e) {
                    if (jQuery(this).val() != '') {
                        $(this).closest('.placeholder').addClass('hasValue');
                    } else {
                        $(this).closest('.placeholder').removeClass('hasValue');
                    }
                }
            });

            $('.placeholder .ginput_container > *').trigger('change');
        }
    }

    showPlaceholder();

    $(document).on('gform_page_loaded', function () {
        showPlaceholder();
    });

    $(window).on('load resize', function(){
        is_header_video_mobile();

    });

    function is_header_video_mobile()
    {
        if($('#header_video').length > 0){
            let screen_size = $(window).width();

            if(screen_size > 768){
                if($('#header_video').attr('src') == ''){
                    $('#header_video').attr('src', $('#header_video').data('src'))
                    $('.video-container').css("background-image", "");
                }

            } else {
                $('#header_video').attr('src', '')
                $('.video-container').css("background-image", "url(" + $('.video-container').data('img') + ")");
            }
        }
    }

    var vc_tta_tabs = document.querySelectorAll('.vc_tta-tabs-position-left');

    if (vc_tta_tabs.length > 0) {
        for (var i = 0; i < vc_tta_tabs.length; i++) {
            let vc_tta_tab = vc_tta_tabs[i];
            let vc_tta_tabs_list = vc_tta_tab.querySelector('.vc_tta-tabs-list')
            let vc_tta_tabs_height = vc_tta_tabs_list.querySelector('.vc_tta-tab:first-child').offsetHeight + 'px';
            let vc_tta_tabs_button = document.createElement('button');

            vc_tta_tabs_button.style.height = vc_tta_tabs_height;
            vc_tta_tabs_button.classList.add('vc_tta_tabs_button');
            vc_tta_tabs_button.innerHTML = '<i class="fa-solid fa-chevron-down"></i>';

            // vc_tta_tab.querySelector('.vc_tta-tabs-container').style.height = vc_tta_tabs_height;
            vc_tta_tabs_list.style.height = vc_tta_tabs_height;
            vc_tta_tab.prepend(vc_tta_tabs_button);

            vc_tta_tab.onclick = function (e) {
                vc_tta_tabs_button = true;

                if (vc_tta_tabs_list.classList.contains('overflow-y-visible')) {
                    vc_tta_tabs_list.classList.remove('overflow-y-visible');

                } else {
                    vc_tta_tabs_list.classList.add('overflow-y-visible');
                }
            };


        }

        $(window).on('resize', function (e) {
            for (var i = 0; i < vc_tta_tabs.length; i++) {
                let vc_tta_tabs_height = vc_tta_tabs[i].querySelector('.vc_tta-tab:first-child').offsetHeight + 'px';
                vc_tta_tabs[i].querySelector('.vc_tta_tabs_button').style.height = vc_tta_tabs_height;
                // vc_tta_tabs[i].querySelector('.vc_tta-tabs-container').style.height = vc_tta_tabs_height;
                vc_tta_tabs[i].querySelector('.vc_tta-tabs-list').style.height = vc_tta_tabs_height;
            }
        })
    }


    function render_videos_modal(selector) {
        let video_url = selector.closest('.vc_gitem-col').find('.vc_gitem-post-meta-field-video_id').text();

        jQuery('body').append('<div class="video_modal_wrapper"><div class="video_modal_container"><div class="video_modal">' +
            '<iframe allow="fullscreen" src="https://www.youtube.com/embed/' + video_url + '?controls=2&autoplay=0&mute=0&loop=1&modestbranding=0&rel=0&showinfo=0"></iframe>' +
            '</div></div></div>');
    }

    if (jQuery('.page-header .hero-title > span').length > 1) {
        window.setInterval(function () {
            let hero_title = jQuery('.page-header .hero-title');
            let current_index = hero_title.find('span:not(.d-none)').index();

            hero_title.toggleClass('aos-animate');

            setTimeout(function () {
                hero_title.find('span').eq(current_index).addClass('d-none');

                if (current_index === (hero_title.find('span').length - 1)) {
                    hero_title.find('span').eq(0).removeClass('d-none');
                } else {
                    hero_title.find('span').eq(current_index + 1).removeClass('d-none');
                }
                hero_title.toggleClass('aos-animate')
            }, 450)

        }, 15000);
    }


}(jQuery));
