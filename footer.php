<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package royalquebec
 */

?>

<footer id="colophon" class="site-footer">
    <section class="vc_section">
        <div class="vc_row justify-content-center d-flex ">
            <div class="vc_col-sm-12 vc_col-lg-2 text-align-center mb-3 footer-logo">
                <?php
                if (is_active_sidebar('footer-sidebar-1')) {
                    dynamic_sidebar('footer-sidebar-1');
                }
                ?>
            </div>
            <div class="vc_col-sm-12 vc_col-lg-5 pl-xl-6 d-flex justify-content-center justify-content-xl-start text-align-center text-align-lg-left">
                <?php
                    if (is_active_sidebar('footer-sidebar-2')) {
                        dynamic_sidebar('footer-sidebar-2');
                    }
                ?>
            </div>
            <div class="vc_col-sm-12 vc_col-md-8 vc_col-lg-5  mw-565 mx-auto">
                <?= wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) ?>

            </div>
        </div>
        <div class="vc_row mt-3">
            <div class="vc_col-sm-12 vc_col-lg-2">

            </div>
            <div class="vc_col-sm-12 vc_col-lg-5 pl-xl-6 mb-2 mb-lg-0">
                <?= wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_id' => 'secondary-navigation', 'menu_class' => 'd-flex justify-content-center justify-content-xl-start']) ?>

            </div>
            <div class="vc_col-sm-12 vc_col-lg-5 mb-2 mb-lg-0">
                <?= wp_nav_menu(['theme_location' => 'social_navigation', 'menu_class' => 'nav justify-content-center justify-content-xl-end mb-2 mb-xl-0']) ?>
            </div>
        </div>
    </section>

    <section class="vc_section copyright">
        <div class="vc_row ">
            <div class="vc_col-sm-12 ">
                <p>Copyright
                    © <?= date("Y") . __(' Club de Golf Royal Québec. Tous droits réservés', 'royalquebec') ?></p>
            </div>

        </div>
    </section>

</footer>

</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
