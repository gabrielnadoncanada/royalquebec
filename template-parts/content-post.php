<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
        <section class="vc_section container section">
            <div class="vc_row">
                <div class="mb-4 pr-md-4 pr-lg-6 wpb_column vc_column_container vc_col-sm-8 vc_col-lg-9 vc_col-md-8">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <h1 class="h3 fw-300 mb-3"><?php echo get_the_title(); ?></h1>
                            <?php
                            the_content();
                            ?>
                        </div>
                    </div>
                </div>

                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-offset-0 vc_col-lg-3 vc_col-md-4 vc_col-has-fill">
                    <div class="vc_column-inner bg-light">
                        <div class="wpb_wrapper">
                            <?php
                            get_sidebar();
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="vc_section section pt-0 container vc_custom_1647366197903">
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="mb-4 wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <h2 class="h4" style="text-align: center;">
                                        <?= __('D’autres nouvelles qui pourraient vous intéresser', 'royalquebec') ?>
                                    </h2>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <?php
                            echo do_shortcode('[vc_basic_grid post_type="post" max_items="3" order="ASC" item="1556" grid_id="vc_gid:1647366838644-a3889d18-34f7-5"]')
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <section class="vc_section section-form bg-light">
        <div class="vc_row wpb_row d-flex justify-content-center vc_row-fluid section container vc_custom_1647366241308">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-8">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h2 class="h4">
                                    <?= __('VOUS DÉSIREZ NOUS ENVOYER VOS COMMENTAIRES<br>OU VOS QUESTIONS ?', 'royalquebec') ?>  </h2>

                            </div>
                        </div>

                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <p><?= __('Remplir le formulaire ci-dessous et nous communiquerons avec vous sous peu s’il
                                        est nécessaire', 'royalquebec') ?></p>

                            </div>
                        </div>

                        <?php
                        echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
