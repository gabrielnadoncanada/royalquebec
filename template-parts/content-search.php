<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package royalquebec
 */

$s = get_query_var('s');
$title = str_ireplace( $s, '<span class="search-instance">'.$s.'</span>', get_the_title() );

$content = custom_search::search_content_highlight(get_the_content());
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="col-md-12">
        <header class="entry-header">

            <a class="color-primary" href="<?= esc_url( get_permalink() ) ?>">
                <small class="search-link"><?= esc_url( get_permalink() ) ?></small>
                <p class="h4 fw-600 h6"><?= $title  ?></p>
            </a>
            <small><?= royalquebec_posted_on();?></small>

        </header>
        <style>
            .search .page-content > * {
                font-size: 90% !important;
            }
        </style>
        <div class="entry-summary">
            <div class="page-content">

                <?= $content ?>
            </div>
        </div>
        <hr>
    </div>

</article>
