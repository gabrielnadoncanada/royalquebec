<?php

if (!get_field('section_hidden')) {
    ?>

    <div class="page-header d-flex flex-wrap justify-content-center parallax-bg" style="">
        <?php
        $type = get_field('banner_type');

        switch ($type):
            case 'image':
                ?>
                <div
                        class="bg"
                        style="background-image: url(<?php echo get_field('background_image')['url'] ?>);"
                        data-depth='0.20'
                ></div>
                <?php break;
            case 'youtube':
                $array = explode('/', get_field('url_youtube'));
                $videoId = end($array);
                $video_url = 'https://www.youtube.com/embed/'.  $videoId .'?controls=0&autoplay=1&mute=1&disablekb=1&loop=1&modestbranding=1&rel=0&showinfo=0'
                ?>
                <div class="video-container" data-img="<?= get_field('image_video_mobile')['url'] ?>">
                    <iframe id="header_video"
                            data-src="<?= $video_url ?>"
                            src=""
                    >

                    </iframe>

                    <div class="disable-control"></div>
                </div>
                <?php break;
            case 'vimeo':
                $array = explode('/', get_field('url_vimeo'));
                $videoId = end($array);
                $video_url = 'https://player.vimeo.com/video/' . $videoId . '?h=470dd1d150&background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0';

                ?>
                <div class="video-container w-100" data-img="<?= get_field('image_video_mobile')['url'] ?>">
                    <div style="padding:56.25% 0 0 0;position:relative;pointer-events: none;">
                        <iframe id="header_video"
                                data-src="<?= $video_url ?>"
                                src=""
                                style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
                                allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <script src="https://player.vimeo.com/api/player.js"></script>
                    <div class="disable-control"></div>
                </div>
                <?php break;
            case 'video':
                ?>
                <div class="video-container w-100" data-img="<?= get_field('image_video_mobile')['url'] ?>">

                    <video height="auto" width="100%" intrinsicsize="900x900" autoplay loop muted fullscreen
                           data-src="<?php echo get_field('video')['url'] ?>">
                        <source src="<?php echo get_field('video')['url'] ?>"
                                type="<?php echo get_field('video')['mime_type'] ?>">
                        <p>Votre navigateur ne prend pas en charge les vidéos HTML5.
                            Voici <a href="myVideo.mp4">un lien pour télécharger la vidéo</a>.</p>
                    </video>
                </div>
                <?php break;
            default:
                ?>
                <div
                        class="bg"
                        style="background-image: url(<?php echo get_template_directory_uri() . '/img/HeaderGolf.jpg' ?>);"
                        data-depth='6.85'
                ></div>
            <?php endswitch; ?>
        <div class="content container ">
            <?php
            if (!get_field('banner_title_hidden')) {
                ?>
                <?php
                if (!get_field('banner_text')) {
                    if (is_search()) {
                        ?>
                        <h1 class="h3 hero-title" data-aos="fade-up" data-aos-duration="450" data-aos-easing="ease-in">
                            <?php
                            printf(esc_html__('Résultats de recherche pour: %s', 'royalquebec'), '<span>' . get_search_query() . '</span>');
                            ?>
                        </h1>
                        <?php
                    } elseif (get_post_type() === 'post') {
                        ?>
                        <p class="h3 hero-title" data-aos="fade-up" data-aos-duration="450" data-aos-easing="ease-in">
                            <?php
                            echo __('Nouvelles', 'royalquebec');
                            ?>
                        </p>
                        <?php
                    } elseif (get_post_type() === 'job') {
                        ?>
                        <p class="h3 hero-title" data-aos="fade-up" data-aos-duration="450" data-aos-easing="ease-in">
                            <?php
                            echo get_the_title(get_the_ID());
                            ?>
                        </p>
                        <?php
                    } else {
                        ?>
                        <h1 class="h3 hero-title" data-aos="fade-up" data-aos-duration="450" data-aos-easing="ease-in">

                            <?php
                            echo get_the_title(get_the_ID());
                            ?>
                        </h1>
                        <?php
                    }
                } else {
                    ?>
                    <p class="h3 hero-title" data-aos="fade-up" data-aos-duration="450" data-aos-easing="ease-in">
                        <?php
                        $banner_texts = get_field('banner_texts');

                        if ($banner_texts) {
                            foreach ($banner_texts as $key => $banner_text) {
                                ?>
                                <span class="<?= $key ? 'd-none' : '' ?>">
                                    <?= $banner_text['banner_text'] ?>
                                </span>
                                <?php
                            }
                        }
                        ?>
                    </p>
                    <?php
                }
                ?>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>

<?php
if (!get_field('breadcrumb_hidden') && class_exists('WPSEO_Options')) {
    ?>
    <section style="background-color: #f3f3f0;">
        <div class="vc_section container">
            <div class="vc_row">
                <div class="vc_col-sm-12 vc_column_container">
                    <div class="vc_column-inner">
                        <?php
                        yoast_breadcrumb('<p id="breadcrumbs" class="my-0 py-1">', '</p>');

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>




