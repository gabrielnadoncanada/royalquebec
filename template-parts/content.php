<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package royalquebec
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    get_template_part( 'template-parts/partials/page-header', '' );
    ?>

	<footer class="entry-footer">
		<?php royalquebec_entry_footer(); ?>
	</footer>
</article>
