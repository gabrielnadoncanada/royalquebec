<?php


/**
 * royalquebec functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package royalquebec
 */

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function royalquebec_setup()
{
    /*
        * Make theme available for translation.
        * Translations can be filed in the /languages/ directory.
        * If you're building a theme based on royalquebec, use a find and replace
        * to change 'royalquebec' to the name of your theme in all the template files.
        */
    load_theme_textdomain('royalquebec', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
        * provide it for us.
        */
    add_theme_support('title-tag');

    /*
        * Enable support for Post Thumbnails on posts and pages.
        *
        * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
        */
    add_theme_support('post-thumbnails');


    // This theme uses wp_nav_menu() in one location.
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'royalquebec'),
        'secondary_navigation' => __('Secondary Navigation', 'royalquebec'),
        'social_navigation' => __('Social Navigation', 'royalquebec')
    ]);

    register_sidebar(array(
        'name' => 'Footer logo',
        'id' => 'footer-sidebar-1',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Footer text',
        'id' => 'footer-sidebar-2',
        'description' => 'Appears in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    /*
        * Switch default core markup for search form, comment form, and comments
        * to output valid HTML5.
        */
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'style',
            'script',
        )
    );

    add_image_size('menu-image', 380, 280, true);

    // Set up the WordPress core custom background feature.
    add_theme_support(
        'custom-background',
        apply_filters(
            'royalquebec_custom_background_args',
            array(
                'default-color' => 'ffffff',
                'default-image' => '',
            )
        )
    );

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support(
        'custom-logo',
        array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        )
    );


}

add_action('after_setup_theme', 'royalquebec_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function royalquebec_widgets_init()
{
    register_sidebar(
        array(
            'name' => esc_html__('Sidebar', 'royalquebec'),
            'id' => 'sidebar-1',
            'description' => esc_html__('Add widgets here.', 'royalquebec'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        )
    );
}

add_action('widgets_init', 'royalquebec_widgets_init');
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function royalquebec_content_width()
{
    $GLOBALS['content_width'] = apply_filters('royalquebec_content_width', 640);
}

add_action('after_setup_theme', 'royalquebec_content_width', 0);


/**
 * Enqueue scripts and styles.
 */
function royalquebec_scripts()
{
    wp_enqueue_style('royalquebec-old', get_template_directory_uri() . '/old.css', array(), 2.0);
    wp_enqueue_style('royalquebec-style', get_stylesheet_uri(), array(), 2.0);
    wp_enqueue_style('royalquebec/slick-theme.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', false, null);
    wp_enqueue_style('royalquebec/slick.css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css', false, null);
    wp_enqueue_style('royalquebec/aos.css', 'https://unpkg.com/aos@2.3.1/dist/aos.css', false, null);
    wp_enqueue_style('royalquebec/lightbox2.css', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css', false, null);
    wp_enqueue_style('js_composer_front', vc_asset_url('css/js_composer.min.css'), array(), false);
    wp_enqueue_script('royalquebec/navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);
    wp_enqueue_script('royalquebec/main', get_template_directory_uri() . '/js/main.js', ['jquery'], _S_VERSION, true);
    wp_enqueue_script('royalquebec/slider', get_template_directory_uri() . '/js/slider.js', array('royalquebec/slick.js'), _S_VERSION, true);
    wp_enqueue_script('royalquebec/slick.js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], null, true);
    wp_enqueue_script('royalquebec/aos.js', 'https://unpkg.com/aos@2.3.1/dist/aos.js', ['jquery'], null, true);
    wp_enqueue_script('royalquebec/lightbox2.js', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js');
//
    wp_style_add_data('royalquebec-style', 'rtl', 'replace');
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'royalquebec_scripts');


function remove_wp_block_library_css()
{
    wp_dequeue_style('wp-block-library');
    wp_dequeue_style('wp-block-library-theme');
    wp_dequeue_style('wc-block-style');
    wp_dequeue_style('global-styles');
}

add_action('wp_enqueue_scripts', 'remove_wp_block_library_css', 100);

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


add_action('wp_footer', function () {
    ?>
    <script>
        (function ($) {
            $(document).ready(function () {
                AOS.init();
            })

        })(jQuery);

    </script>
    <?php
});


require get_template_directory() . '/inc/modules/job.php';
require get_template_directory() . '/inc/modules/partner.php';
require get_template_directory() . '/inc/modules/teacher.php';
require get_template_directory() . '/inc/modules/course.php';
require get_template_directory() . '/inc/modules/page.php';
require get_template_directory() . '/inc/modules/menu.php';
require get_template_directory() . '/inc/modules/video.php';
require get_template_directory() . '/inc/modules/camp.php';
require get_template_directory() . '/inc/modules/notification.php';
require get_template_directory() . '/inc/search.php';
require get_template_directory() . '/inc/vc-templates.php';


$attributes = array(
    'type' => 'textfield',
    'heading' => "Extra Attr",
    'param_name' => 'extra_attr',
    'description' => __("Extra Attr", "royalquebec")
);


vc_add_param('vc_column_inner', $attributes);
vc_add_param('vc_column', $attributes);

add_filter('the_content', 'do_shortcode');


add_shortcode('acf_form', 'acf_frontend_form');

function acf_frontend_form($atts)
{
    ob_start();

    $atts = shortcode_atts(array(
        'post_id' => false,
        'fields' => false,
        'submit_value' => __("Update", 'acf'),
        'updated_message' => __("Post updated", 'acf'),
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'field_el' => 'div',
        'uploader' => 'wp',
        'honeypot' => true,
        'html_updated_message' => '<div id="message" class="updated"><p class="mb-0">%s</p></div>',
    ), $atts, 'acf_form');

    if (!empty($_GET['update']) && $atts['updated_message']) {
        if ($atts['fields'] === $_GET['update']) {
            printf($atts['html_updated_message'], $atts['updated_message']);
            if ($atts['post_id']) {
                echo '<div class="mb-2">';
                echo '<a target="blank" href="' . get_permalink($atts['post_id']) . '">';
                echo __('Voir le résultat');
                echo '</a><br>';
                echo '</div>';
            }
        }
    }

    echo '<b>' . __('Mise à jour le: ', 'royalquebec') . '</b><br>';

    echo do_shortcode('[menu_notifications meta_key="' . $atts['fields'] . '"]');

    $atts['return'] = add_query_arg('update', $atts['fields'], acf_get_current_url());


    if ($atts['fields']) {
        $atts['fields'] = array($atts['fields']);
    }

    acf_form_head();

    acf_form($atts);

    $html = ob_get_contents();

    ob_end_clean();

    return $html;
}


add_filter('gform_pre_send_email', function ($email, $message_format, $notification) {

    if ($_POST['input_15']) {
        $email['headers']['to'] = $_POST['input_15'];
        $email['to'] = $_POST['input_15'];

    }
    if ($_POST['input_34_1'] === 'Envoyer une copie à votre courriel' && !empty($_POST['input_18'])) {
        $email['headers']['Bcc'] = 'Bcc: ' . $_POST['input_18'];
    }

    if ($_POST['input_12_1'] === 'Envoyer une copie à votre courriel' && !empty($_POST['input_4'])) {
        $email['headers']['Bcc'] = 'Bcc: ' . $_POST['input_4'];
    }
    if ($_POST['input_9_1'] === 'Envoyer une copie à votre courriel' && !empty($_POST['input_3'])) {
        $email['headers']['Bcc'] = 'Bcc: ' . $_POST['input_3'];
    }
    return $email;
}, 10, 3);


add_filter('wpseo_breadcrumb_links', 'wpseo_breadcrumb_add_woo_shop_link');

function wpseo_breadcrumb_add_woo_shop_link($links)
{
    global $post;

    if (ICL_LANGUAGE_CODE === 'en') {
        $links[0]['text'] = 'Home';
    }
    if (get_post_type() == 'job') {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/' . __('emplois', 'royalquebec'),
            'text' => 'Emplois',
        );

        array_splice($links, 1, -2, $breadcrumb);
    }

    if (get_post_type() == 'post') {
        $breadcrumb[] = array(
            'url' => get_site_url() . '/' . __('nouvelles', 'royalquebec'),
            'text' => 'Nouvelles',
        );

        array_splice($links, 1, -2, $breadcrumb);
    }

    return $links;
}


function add_the_table_button($buttons)
{
    array_push($buttons, 'separator', 'table');
    return $buttons;
}

add_filter('mce_buttons', 'add_the_table_button');

function add_the_table_plugin($plugins)
{
    $plugins['table'] = get_template_directory_uri() . '/tinymce-plugins/table/plugin.min.js';
    return $plugins;
}

add_filter('mce_external_plugins', 'add_the_table_plugin');



