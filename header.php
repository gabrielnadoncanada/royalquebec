<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package royalquebec
 */

?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link
                href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
                rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">


      <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <div class="search_form flex justify-content-center">

      <?php get_search_form(); ?>
    </div>
    <header id="masthead" class="site-header">
        <div class="site-branding">
          <?php
          the_custom_logo();
          ?>
        </div><!-- .site-branding -->

        <nav id="main-navigation" class="main-navigation">
            <button class="menu-toggle" aria-controls="primary-navigation" aria-expanded="false">
                <img src="/wp-content/themes/royalquebec/img/burger.svg" alt=""> <span>Menu <i
                            class="fa-solid fa-chevron-down"></i></span>
            </button>
            <div class="menu-wrapper">
              <?= wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_id' => 'secondary-navigation']) ?>
              <?= wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_id' => 'primary-navigation']) ?>
              <?= wp_nav_menu(['theme_location' => 'social_navigation', 'menu_id' => 'social-navigation']) ?>
            </div>

        </nav>


        <nav id="site-navigation" class="main-navigation">
          <?= wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_id' => 'secondary-navigation']) ?>


            <span class="meteo"><a target="_blank"
                        href="https://www.meteomedia.com/ca/previsions-meteo-36-heures/quebec/club-de-golf-royal-quebec"><img
                            src="/wp-content/themes/royalquebec/img/meteo.svg" alt=""></a></span>
            <span class="search_toggle"><img src="/wp-content/themes/royalquebec/img/search.svg" alt=""></span>
        </nav><!-- #site-navigation -->
    </header><!-- #masthead -->

<?php

