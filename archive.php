<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package royalquebec
 */

get_header();
?>

    <main id="primary" class="site-main">

        <?php if (have_posts()) : ?>

            <?php
            /* Start the Loop */
            get_template_part('template-parts/partials/page-header', '');

            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="entry-content ">

                    <?php


                    $term_id = get_queried_object()->term_taxonomy_id;

                    $wpb_grid = '[vc_section el_class="container section-news section border-bottom" css=".vc_custom_1647363967938{padding-top: 0px !important;}"][vc_row][vc_column width="2/3" el_class="mb-4 pr-md-4 pr-lg-6" offset="vc_col-lg-9 vc_col-md-8"]';
                    $wpb_grid .= '[vc_basic_grid post_type="post" max_items="10" item="1556" grid_id="vc_gid:1647360213948-cc2a75ab-dfc8-10" taxonomies="' . $term_id . '"]';
                    $wpb_grid .= '[/vc_column][vc_column width="1/3" offset="vc_col-lg-offset-0 vc_col-lg-3 vc_col-md-4" el_class="bg-light"]';
                    $wpb_grid .= '[vc_widget_sidebar el_class="pt-2 px-md-1" sidebar_id="sidebar-1"][/vc_column][/vc_row][/vc_section]';

                    echo do_shortcode($wpb_grid);
                    ?>

                </div>

            </article>


            <?php



        else :

            get_template_part('template-parts/content', 'none');

        endif;
        ?>

    </main><!-- #main -->

<?php
get_footer();
