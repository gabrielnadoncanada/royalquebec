<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package royalquebec
 */

get_header();
?>

	<main id="primary" class="site-main">
		<section class="error-404 not-found container section">
			<div class="page-content">
                <h1 class="page-title mb-2"><?php esc_html_e( 'Rien n\'a été trouvé.', 'royalquebec' ); ?></h1>
				<p><?php esc_html_e( 'Il semble que nous ne puissions pas trouver ce que vous cherchez.', 'royalquebec' ); ?></p>
                <a href="<?= get_home_url() ?>" class="btn btn-accent">Retourner à l'accueil</a>
			</div>
		</section>
        </main>
<?php
get_footer();
