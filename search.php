<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package royalquebec
 */

get_header();
?>


    <main id="primary" class="site-main">
        <?php get_template_part('template-parts/partials/page-header', ''); ?>
        <section class="vc_section section ">
            <div class="container">
                <?php if (have_posts()) : ?>
                    <?php
                    while (have_posts()) :
                        the_post();
                        get_template_part('template-parts/content', 'search');
                    endwhile;
                else :
                    get_template_part('template-parts/content', 'none');
                endif;
                ?>
            </div>
        </section>
    </main><!-- #main -->

<?php

get_footer();
