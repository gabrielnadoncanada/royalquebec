<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class job
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_shortcode('jobs', [$this, 'jobs_shortcode']);
        add_filter('gform_pre_render', [$this, 'job_populate_choices']);
        add_filter('gform_pre_validation', [$this, 'job_populate_choices']);
        add_filter('gform_pre_submission_filter', [$this, 'job_populate_choices']);
        add_filter('gform_admin_pre_render', [$this, 'job_populate_choices']);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Emplois', 'job'),
            'singular_name' => __('Emploi', 'job'),
            'menu_name' => __('Emplois', 'job'),
            'all_items' => __('Tout les Emplois', 'job'),
            'view_item' => __('Voir tout les Emplois', 'job'),
            'add_new_item' => __('Ajouter un nouveau Emploi', 'job'),
            'add_new' => __('Ajouter', 'job'),
            'edit_item' => __('Ã‰diter', 'job'),
            'update_item' => __('Modifier', 'job'),
            'search_items' => __('Rechercher', 'job'),
            'not_found' => __('Aucun rÃ©sultat', 'job'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'job'),
        );

        $args = array(
            'label' => __('Emploi', 'job'),
            'description' => __('Emplois', 'job'),
            'rewrite' => ['slug' => __('emplois', 'job')],
            'labels' => $labels,
            'supports' => array('title'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'publicly_queryable' => false,
            'has_archive' => false,
        );

        register_post_type('job', $args);

        acf_add_local_field_group($this->job_field()->build());
    }


    /**
     * Output Jobs List
     */
    public function jobs_shortcode()
    {
        ob_start();

        $args = array(
            'post_type' => 'job',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);

        $content = '[vc_tta_tour]';


        while ($the_query->have_posts()) : $the_query->the_post();

            $details_obj = get_field_object('details');


            $content .= '[vc_tta_section title="' . get_the_title() . '<br><em>' . get_the_date('j M Y') . '</em>" tab_id="' . get_the_ID() . '"]';
            $content .= '<h3 class="text-accent mb-3">' . get_the_title() . '</h3>';

            $ctn = 0;

            foreach ($details_obj['value'] as $key => $value) {
                if (!empty($value)) {
                    $content .= '<h2 class="my-2 h6"><b>' . $details_obj['sub_fields'][$ctn]['label'] . '</b></h2>';
                    $content .= $value;
                }
                $ctn++;
            }


            $content .= '[/vc_tta_section]';

        endwhile;

        $content .= '[/vc_tta_tour]';

        echo do_shortcode($content);

        wp_reset_postdata();

        return ob_get_clean();
    }

    /**
     * register our job acf field
     * @return FieldsBuilder
     */
    public function job_field()
    {
        $job = new FieldsBuilder('job');

        $job
            ->setLocation('post_type', '==', 'job');

        $job
            ->addGroup('details')
            ->addTab('general')
            ->addWysiwyg('Description', ['label' => __('Responsabilités', 'job'),])
            ->addTab('Exigences')
            ->addWysiwyg('Exigences', ['label' => __('Exigences', 'job'),])
            ->addTab('Competences / qualifications')
            ->addWysiwyg('Competences / qualifications', ['label' => __('Compétences/qualifications', 'job'),])
            ->addTab('Entente contractuelle')
            ->addWysiwyg('Entente contractuelle', ['label' => __('Entente contractuelle', 'job'),])
            ->addTab('Autres information')
            ->addWysiwyg('Autre information', ['label' => __('Autres informations', 'job'),])
            ->endGroup();


        return $job;
    }


    public function gform_field_value_jobs()
    {
        return wp_list_pluck(get_posts(array('post_type' => 'job')), 'post_title');
    }

    function job_populate_choices($form) {
        foreach ($form['fields'] as &$field) {
            if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-jobs' ) === false ) {
                continue;
            }

            $posts = get_posts( 'post_type=job&numberposts=-1' );

            $field->choices = [];

            foreach ( $posts as $post ) {
                $field->choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
            }

            $field->placeholder = __('Choisissez un emploi', 'job');
        }
        return $form;
    }
}

new job();

