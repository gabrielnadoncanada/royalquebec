<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class course
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Parcours', 'course'),
            'singular_name' => __('Parcour', 'course'),
            'menu_name' => __('Parcours', 'course'),
            'all_items' => __('Tout les Parcours', 'course'),
            'view_item' => __('Voir tout les Parcours', 'course'),
            'add_new_item' => __('Ajouter un nouveau Parcour', 'course'),
            'add_new' => __('Ajouter', 'course'),
            'edit_item' => __('Éditer', 'course'),
            'update_item' => __('Modifier', 'course'),
            'search_items' => __('Rechercher', 'course'),
            'not_found' => __('Aucun résultat', 'course'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'course'),
        );

        $args = array(
            'label' => __('Parcours', 'course'),
            'description' => __('Parcours', 'course'),
            'labels' => $labels,
            'supports' => array('title', 'thumbnail'),
            'show_in_rest' => true,
            'hierarchical' => false,
            'publicly_queryable'  => false,
            'public' => true,
            'has_archive' => false,
        );
        register_post_type('course', $args);
        $this->init_acf();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->course_field()->build());
    }

    public function course_field()
    {
        $config = (object)[
            'ui' => 1,
            'wrapper' => ['width' => 30],
        ];

        $course = new FieldsBuilder('course');

        $course
            ->setLocation('post_type', '==', 'course');

        $course
            ->addRepeater('holes', ['min' => 1, 'layout' => 'block'])
            ->addTab('content')
            ->addSelect('custom_number', [
                'choices' => ['3', '4', '5'],
            ])
            ->addImage('img_1', [
                'wrapper' => $config->wrapper,
                'choices' => ['3', '4', '5'],
            ])
            ->addImage('img_2', [
                'wrapper' => $config->wrapper,
                'choices' => ['3', '4', '5'],
            ])
            ->addImage('img_3', [
                'wrapper' => $config->wrapper,
                'choices' => ['3', '4', '5'],
            ]);


        return $course;
    }
}

new course();


