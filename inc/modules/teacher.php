<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class teacher
{
    public function __construct()
    {
        add_action("init", [$this, "init"]);

        add_shortcode('teachers', [$this, 'teachers']);
        add_shortcode('teachers_link', [$this, 'teachers_link']);
        add_shortcode('teachers_list', [$this, 'teachers_list']);
    }

    public function init()
    {
        $labels = array(
            'name' => __('Enseignants', 'teacher'),
            'singular_name' => __('Enseignant', 'teacher'),
            'menu_name' => __('Enseignants', 'teacher'),
            'all_items' => __('Tout les Enseignants', 'teacher'),
            'view_item' => __('Voir tout les Enseignants', 'teacher'),
            'add_new_item' => __('Ajouter un nouveau Enseignant', 'teacher'),
            'add_new' => __('Ajouter', 'teacher'),
            'edit_item' => __('Éditer', 'teacher'),
            'update_item' => __('Modifier', 'teacher'),
            'search_items' => __('Rechercher', 'teacher'),
            'not_found' => __('Aucun résultat', 'teacher'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'teacher'),
        );

        $args = array(
            'label' => __('Enseignant', 'teacher'),
            'description' => __('Enseignants', 'teacher'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('enseignants', 'teacher')],
            'supports' => array('title', 'thumbnail'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'publicly_queryable'  => false,
            'public' => true,
            'has_archive' => true,
        );
        register_post_type('teacher', $args);

        $this->init_acf();
    }

    public function teachers()
    {
        ob_start();
        $args = array(
            'post_type' => 'teacher',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);

        echo "<div  class='teacher container vc_row wpb_row vc_row-fluid d-flex'>";

        while ($the_query->have_posts()) : $the_query->the_post();

            $details = get_field('details');

            $accordions = '[vc_tta_accordion c_icon="chevron" c_position="right" active_section="999" collapsible_all="true"]';


            $trans = [
                'certifications' => __('Certifications', 'teacher'),
                'formations' => __('Formations', 'teacher'),
                'experiences' => __('Expériences', 'teacher'),
                'technologies' => __('Technologies', 'teacher'),
                'services' => __('Services', 'teacher'),

            ];

            foreach ($details as $key => $item) {
                if (!$item) {
                    continue;
                }


                $accordions .= '[vc_tta_section title="' . $trans[$key] . '" tab_id="' . $key . '"]';


                $accordions .= '[vc_column_text]' . $item . '[/vc_column_text]';
                $accordions .= '[/vc_tta_section]';

            }

            $accordions .= '[/vc_tta_accordion]';


            ?>
            <div class="vc_col-sm-12 vc_col-sm-6">

                <div class="card">
                    <div class="card-header">
                        <img src="<?= wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>" alt="">
                        <h3 class="h7 card-title"><?= get_the_title() ?></h3>
                    </div>
                    <div class="card-body">
                        <div class="mt-0 d-flex border-bottom pb-2">
                            <a class="btn btn-accent mb-1 mb-lg-0 mr-auto" href="#teacher"
                               data-title="<?= __('Formulaire de réservation avec ', 'royalquebec') . get_the_title()?>"
                               data-id="<?= get_the_ID()?>"
                               data-email="<?= get_field('courriel', get_the_ID())?>"
                            >
                                <?= __('RÉSERVATIONS ET INFOS', 'royalquebec') ?>
                            </a>

                            <?php
                            if(get_field('videos') && !empty(get_field('videos'))){

                                    ?>
                                <a href="<?= get_field('videos')['url'] ?>" target="_blank"
                                   class="my-auto d-flex align-items-center color-black">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28"
                                         viewBox="0 0 32.198 32.198">
                                        <g id="Group_388" data-name="Group 388"
                                           transform="translate(-424.768 -3019.727)">
                                            <circle id="Ellipse_1" data-name="Ellipse 1" cx="16.099"
                                                    cy="16.099" r="16.099"
                                                    transform="translate(424.768 3019.727)" fill="#D22333"/>
                                            <path id="Path_7" data-name="Path 7"
                                                  d="M436.194,3031.3v10.164a.436.436,0,0,0,.633.389l10.006-5.082a.437.437,0,0,0,0-.778l-10.006-5.082A.436.436,0,0,0,436.194,3031.3Z"
                                                  fill="#fff"/>
                                        </g>
                                    </svg>
                                    <span class="pl-1 my-auto h7"><b><?= __('Mes vidéos', 'royalquebec') ?></b></span></a>
                                    <?php
                            }
                            ?>

                        </div>

                        <?php
                        echo do_shortcode($accordions);
                        ?>
                    </div>
                </div>


            </div>
        <?php


        endwhile;

        echo "</div>";
        $box_title = __('FORMULAIRE DE RÉSERVATION AVEC <br> ', 'royalquebec');
//        $box_title .= get_the_title();
        $shortcode_content = '[modal listener_id="#teacher" box_title="' . $box_title . '"]';


        $shortcode_content .= '[gravityform id="1" title="false" description="false" ajax="true"]';
        $shortcode_content .= '[/modal]';
        echo do_shortcode($shortcode_content);
        ?>


        <?php

        wp_reset_postdata();
        return ob_get_clean();
    }

    public function teachers_link()
    {
        ob_start();
        $args = array(
            'post_type' => 'teacher',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);
        echo '<div class="teachers_link">';
        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
            <a data-action="modal" class="btn btn-accent mx-1 mb-2" href="#teacher"
               data-title="<?= __('FORMULAIRE DE RÉSERVATION AVEC <br> ', 'royalquebec') . get_the_title()?>"
               data-id="<?= get_the_ID()?>"
               data-email="<?= get_field('courriel', get_the_ID())?>"
              >
                <?= get_the_title() ?>
            </a>
        <?php
        endwhile;
        echo '</div>';


        $box_title = __('FORMULAIRE DE RÉSERVATION AVEC <br> ', 'royalquebec');
//        $box_title .= get_the_title();
        $shortcode_content = '[modal listener_id="#teacher" box_title="' . $box_title . '"]';
        $shortcode_content .= '[gravityform id="1" title="false" description="false" ajax="true"]';
        $shortcode_content .= '[/modal]';
        echo do_shortcode($shortcode_content);
        ?>


        <?php
        wp_reset_postdata();
        return ob_get_clean();
    }

    public function teachers_list()
    {
        ob_start();
        $args = array(
            'post_type' => 'teacher',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);


        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
            <span><?= get_the_title() ?><br></span>
        <?php
        endwhile;

        ?>

        <?php
        wp_reset_postdata();
        return ob_get_clean();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->teacher_field()->build());
    }

    public function teacher_field()
    {
        $teacher = new FieldsBuilder('teacher');

        $teacher
            ->setLocation('post_type', '==', 'teacher');

        $teacher
            ->addEmail('courriel')
            ->setRequired()
            ->addLink('videos')
            ->addGroup('details')
            ->addTab('certifications')
            ->addWysiwyg('certifications')
            ->addTab('formations')
            ->addWysiwyg('formations')

            ->addTab('experiences')
            ->addWysiwyg('experiences')
            ->addTab('technologies')
            ->addWysiwyg('technologies')
            ->addTab('services')
            ->addWysiwyg('services')
            ->endGroup();


        return $teacher;
    }
}

new teacher();


