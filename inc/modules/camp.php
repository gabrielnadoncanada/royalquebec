<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class camp
{
    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_shortcode('camp_list', [$this, 'camp_list']);
        add_filter('gform_pre_render', [$this, 'camp_populate_choices']);
        add_filter('gform_pre_validation', [$this, 'camp_populate_choices']);
        add_filter('gform_pre_submission_filter', [$this, 'camp_populate_choices']);
        add_filter('gform_admin_pre_render', [$this, 'camp_populate_choices']);
    }

    public function init()
    {
        $camp = new FieldsBuilder('camp');

        $camp
            ->setLocation('page', '==', '794');


        $camp
            ->addRepeater('weeks')
            ->addDatePicker('date_start', [
                'label' => 'Date de début',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
                'return_format' => 'j',
            ])
            ->setRequired()
            ->addDatePicker('date_end', [
                'label' => 'Date de fin',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => [],
                'wrapper' => [
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ],
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
                'return_format' => 'j F',
            ])
            ->setRequired()
            ->addTrueFalse('avalaible', [
                'default_value' => true
            ])
            ->endRepeater();

        acf_add_local_field_group($camp->build());
    }

    public function camp_list()
    {
        ob_start();

        $weeks = get_field('weeks', 794);

        if($weeks){
            echo '<div class="vc_row d-flex align-items-xs-stretch">';
            foreach ($weeks as $key => $week){
                ?>
                <div class="week vc_col-xs-12 vc_col-sm-6 vc_col-md-4 px-1 mb-3 ">
                    <div class="bg-light px-2 py-4 py-md-8 d-flex align-items-xs-center justify-content-center h-100">
                        <p class="h4 text-transform-uppercase fw-500 text-align-center px-1 mb-3 w-100">
                            <?= __('Semaine<br>du ', 'royalquebec') . $week['date_start'] ?><br>
                            <?= __('au ', 'royalquebec') .  $week['date_end'] ?>
                        </p>
                        <?php
                        if($week['avalaible']){
                            echo '<a target="_blank" href="https://academieroyalquebec.com/camps-dt-2022/camp-dt-semaine-1-27-1-juillet-2022" class="btn btn-primary">'. __('Inscrivez-vous', 'royalquebec') .'</a>';
                        } else {
                            echo '<a target="_blank" href="https://academieroyalquebec.com/camps-dt-2022/camp-dt-semaine-1-27-1-juillet-2022" class="disabled btn ">'. __('Complet', 'royalquebec') .'</a>';
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            echo '</div>';
        }

        return ob_get_clean();
    }

    function camp_populate_choices($form) {
        foreach ($form['fields'] as &$field) {
            if ($field->inputName == 'weeks') {

                $weeks = get_field('weeks',794);

                $choices = [];
                $inputs = [];
                $input_id = 1;

                foreach ($weeks as $week) {
                    if($week['avalaible']){
                        $text = __('Semaine du ', 'royalquebec') . $week['date_start'];
                        $text .= __(' au ', 'royalquebec') .  $week['date_end'];
                        $choices[] = ['text' => $text, 'value' => $week->ID];
                        $inputs[] = ['label' => $text, 'id' => $field->id . '.' . $input_id];
                        $input_id++;
                    }
                }

                // Set choices to field
                $field->choices = $choices;
                $field->inputs = $inputs;
            }
        }
        return $form;
    }
}

new camp();
