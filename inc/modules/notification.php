<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class notification
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
//        add_shortcode('notifications', [$this, 'notifications']);
//        add_shortcode('notifications_menu', [$this, 'notifications_menu']);
        add_action('updated_post_meta', [$this, "menu_notification"], 0, 4);
        add_shortcode('menu_notifications', [$this, 'menu_notifications']);
        add_filter('wp_nav_menu_objects', [$this, "notifications_menu"], 10, 2);
        add_action('transition_post_status', [$this, 'notify'], 10, 3);

    }

    public function init()
    {

        $labels = [
            'name' => __('Notifications', 'notification'),
            'singular_name' => __('Notification', 'notification'),
            'menu_name' => __('Notifications', 'notification'),
            'all_items' => __('Tout les Notifications', 'notification'),
            'view_item' => __('Voir tout les Notifications', 'notification'),
            'add_new_item' => __('Ajouter un nouveau Notification', 'notification'),
            'add_new' => __('Ajouter', 'notification'),
            'edit_item' => __('Éditer', 'notification'),
            'update_item' => __('Modifier', 'notification'),
            'search_items' => __('Rechercher', 'notification'),
            'not_found' => __('Aucun résultat', 'notification'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'notification'),
        ];

        $args = [
            'label' => __('Notification', 'notification'),
            'description' => __('Notifications', 'notification'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('notifications', 'notification')],
            'supports' => ['title'],
            'show_in_rest' => false,
            'publicly_queryable' => false,
            'hierarchical' => false,
            'exclude_from_search' => true,
            'public' => true,
            'has_archive' => false,
        ];
        register_post_type('notification', $args);

        $this->init_acf();
    }

    public function notifications()
    {
        ob_start();
        $args = [
            'post_type' => 'notification',
            'posts_per_page' => -1,
        ];

        $the_query = new WP_Query($args);

        echo "<div class='container vc_row wpb_row vc_row-fluid d-flex jc-center notifications'><div class=\"slick\">";
        ?>

        <?php

        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
            <div class="slide">
                <a target="_blank" rel="noopener" href="<?= get_field('url'); ?>">
                    <?= wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()), ['auto', 82]) ?>
                </a>

            </div>
        <?php

        endwhile;

        echo "</div>";

        wp_reset_postdata();
        return ob_get_clean();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->notification_field()->build());
    }

    public function notification_field()
    {
        $notification = new FieldsBuilder('notification');

        $notification
            ->setLocation('post_type', '==', 'notification');

        $notification
            ->addGroup('notification')
            ->addSelect('notification_type', [
                'label' => __('Type de notification', 'royalquebec'),
                'choices' => [
                    ['post_update' => __('Publication mise à jour', 'royalquebec')],
                    ['field_update' => __('Valeur du champ personnalisé mise à jour', 'royalquebec')],
                    ['post_type_create' => __('Création d\'une nouvelle publication', 'royalquebec')]
                ]])
            ->addPostObject('post id', [
                'label' => __('Publication ciblée', 'royalquebec'),
                'return_format' => 'id',
            ])
            ->addSelect('post_type', [
                'label' => __('Type de publication', 'royalquebec'),
                'choices' => get_post_types(),
                'conditional_logic' => [
                    [
                        'field' => 'notification_type',
                        'operator' => '==',
                        'value' => 'post_type_create',
                    ],
                ],
            ])
            ->addRepeater('meta_keys', [
                'conditional_logic' => [
                    [
                        'field' => 'notification_type',
                        'operator' => '==',
                        'value' => 'field_update',
                    ],
                ]
            ])
            ->addText('meta_key', [
                'label' => __('Champ personnalisé', 'royalquebec'),
            ])
            ->addNumber('time_updated', [
                'label' => 'time_updated',
                'conditional_logic' => [
                    [
                        'field' => 'notification_type',
                        'operator' => '==',
                        'value' => 'field_update',
                    ],
                ],
            ])
            ->endRepeater()
            ->addNumber('time_notification', [
                'label' => __('Durée de vie de la notification(en jour)', 'royalquebec'),
            ])
            ->endGroup();

        return $notification;
    }

    public function notifications_menu($items, $args)
    {
        global $post;

        $notifications_fields = $this->get_notifications_fields();

        $post_meta_count = array_count_values(wp_list_pluck($notifications_fields, 'post id'));

        $datas = array();

        foreach ($post_meta_count as $key => $post_count) {
            $datas[$key] = array(
                'id' => $key,
                'children_count' => $post_count,
                'children' => array()
            );
        }

        foreach ($notifications_fields as $notification) {
            array_push($datas[$notification['post id']]['children'], $notification);
        }

        foreach ($items as &$item) {
            if ($item->object == 'page' && array_key_exists($item->object_id, $datas)) {
                $ctn = 0;

                foreach ($datas[$item->object_id]['children'] as $key => $data) {
                    switch ($data['notification_type']) {
                        case 'post_update':
                            $cookie_name = 'notification_' . $data['post id'];

                            $last_modified_time = get_post_modified_time('U', true, $data['post id']);

                            if (isset($_COOKIE[$cookie_name])) {
                                if ($_COOKIE[$cookie_name] != $last_modified_time) {
                                    unset($_COOKIE[$cookie_name]);
                                    setcookie($cookie_name, null, -1, '/');
                                    $ctn++;
                                }
                            } else {
                                if ($data['post id'] == $post->ID) {
                                    setcookie($cookie_name, $last_modified_time, current_time('timestamp') + (86400 * $data['time_notification']), "/");
                                }
                                $ctn++;
                            }
                            break;
                        case 'field_update':
                            foreach ($data['meta_keys'] as $meta_key) {
                                $cookie_name = 'notification_' . $data['post id'] . '_' . $meta_key['meta_key'];

                                if (isset($_COOKIE[$cookie_name])) {
                                    if (!empty($meta_key['time_updated'])) {
                                        if ($_COOKIE[$cookie_name] !== $meta_key['time_updated']) {
                                            unset($_COOKIE[$cookie_name]);
                                            setcookie($cookie_name, null, -1, '/');
                                            $ctn++;
                                        }
                                    }

                                } else {
                                    if ($data['post id'] == $post->ID) {
                                        if (empty($meta_key['time_updated'])) {
                                            $meta_key['time_updated'] = current_time('timestamp');
                                        }
                                        setcookie($cookie_name, $meta_key['time_updated'], current_time('timestamp') + (86400 * $data['time_notification']), "/");
                                    }
                                    $ctn++;
                                }
                            }


                            break;
                        case 'post_type_create':
                            $today = getdate();

                            $args = array(
                                'post_type' => $data['post_type'],
                                'posts_per_page' => -1,
                                'date_query' => array(
                                    array(
                                        'year' => $today['year'],
                                        'month' => $today['mon'],
                                        'day' => $today['mday'],
                                    ),
                                ),
                            );

                            $custom_query = new WP_Query($args);
                            $today_new_post = $custom_query->post_count;

                            $cookie_name = 'notification_' . $data['post id'] . '_' . $data['post_type'];

                            if (isset($_COOKIE[$cookie_name])) {
                                if ($_COOKIE[$cookie_name] != $today_new_post) {
                                    unset($_COOKIE[$cookie_name]);
                                    setcookie($cookie_name, null, -1, '/');
                                    if ($today_new_post) {
                                        $ctn += $today_new_post;
                                    }
                                }
                            } else {
                                $data['time_updated'] = $today_new_post;
                                if ($data['post id'] == $post->ID) {
                                    setcookie($cookie_name, $data['time_updated'], current_time('timestamp') + (86400 * $data['time_notification']), "/");
                                }
                                $ctn += $today_new_post;
                            }
                            break;
                    }
                }

                if ($ctn) {
                    $item->title = $item->title . '<span class="red-dot">' . $ctn . '</span>';
                }
            }
        }

        return $items;
    }

    public function menu_notification($meta_id, $post_id, $meta_key, $_meta_value)
    {
        $notifications = $this->get_notifications_fields();

        if (is_array($notifications) && count($notifications) > 0) {
            foreach ($notifications as $notification) {
                if ($post_id == $notification['post id']) {
                    foreach ($notification['meta_keys'] as $key => $metakey) {
                        if ($meta_key == $metakey['meta_key']) {
                            $notification['meta_keys'][$key]['time_updated'] = current_time('timestamp');
                            update_field('notification', $notification, $notification['notification_id']);
                        }
                    }
                }
            }
        }
    }

    public function get_notifications_ids()
    {
        $args = array(
            'numberposts' => -1,
            'post_type' => 'notification',
            'fields' => 'ids'
        );

        return get_posts($args);
    }


    public function get_notifications_fields()
    {
        $notifications_ids = $this->get_notifications_ids();

        $notifications_fields = [];

        foreach ($notifications_ids as $notification_id) {
            $notification = get_field('notification', $notification_id);
            $notification['notification_id'] = $notification_id;
            $notifications_fields[] = $notification;
        }

        return $notifications_fields;
    }

    public function menu_notifications($atts)
    {
        $atts = shortcode_atts(array('meta_key' => ''), $atts, 'menu_notifications');


        if (!is_admin() && !empty($atts['meta_key'])) {

            $notifications = $this->get_notifications_fields();
            foreach ($notifications as $notification) {
                if($notification['meta_keys']){
                    foreach ($notification['meta_keys'] as $key => $metakey) {
                        if ($atts['meta_key'] == $metakey['meta_key']) {
                            $date_format = get_option( 'date_format' );
                            $time_format = get_option( 'time_format' );
                            return __('Mise à jour le ', 'royalquebec') . date_i18n( $date_format.', '.$time_format, $metakey['time_updated']);
                        }
                    }
                }
            }
        }
    }

    public function notify($new_status, $old_status, $post)
    {
        if ($new_status == 'publish' && $new_status != $old_status) {
            $notifications = $this->get_notifications_ids();

            $ctn = 1;
            foreach ($notifications as $notification) {
                if ($notification['notification_type'] == 'post_type_create') {
                    if ($notification['notification_type'] = $post->post_type) {
                        $today = getdate();

                        $args = array(
                            'post_type' => $notification['post_type'],
                            'posts_per_page' => -1,
                            'date_query' => array(
                                array(
                                    'year' => $today['year'],
                                    'month' => $today['mon'],
                                    'day' => $today['mday'],
                                ),
                            ),
                        );

                        $custom_query = new WP_Query($args);

                        update_sub_field(array('notifications', $ctn, 'time_updated'), $custom_query->post_count, 'options');
                    }
                }
                $ctn++;
            }
            return $post->ID;
        }
    }
}

new notification();
