<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class page
{
    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {
        $page = new FieldsBuilder('page');

        $page
            ->setLocation('post_type', '==', 'page')
            ->or('post_type', '==', 'post');

        $page
            ->addTab('page header')
            ->addTrueFalse('title_hidden')
            ->addTrueFalse('section_hidden')
            ->addTab('page breadcrumb')
            ->addTrueFalse('breadcrumb_hidden');

        $page
            ->addTab('Banner')
            ->addSelect('banner_type', array(
                'label' => __('Type de bannière', 'royalquebec'),
                'choices' => array(
                    'default' => 'Default',
                    'video' => 'Video',
                    'image' => 'Image',
                    'youtube' => 'Youtube',
                    'vimeo' => 'Vimeo',
                )
            ))
            ->addRepeater('banner_texts')
            ->addTextarea('banner_text', array(
                'new_lines' => 'br',
                'label' => __('Texte de la bannière', 'royalquebec'),
            ))
            ->endRepeater();

        $page
            ->addFile('video')
            ->conditional('banner_type', '==', 'video');

        $page
            ->addImage('background_image')
            ->conditional('banner_type', '==', 'image');

        $page
            ->addUrl('url_youtube', array('instructions' => 'Utilisez le bouton partager de youtube pour récupérer le lien de la vidéo.
Ex: https://youtu.be/BHACKCNDMW8'))
            ->conditional('banner_type', '==', 'youtube');

        $page
            ->addUrl('url_vimeo', array('instructions' => 'Utilisez le bouton partager de vimeo pour récupérer le lien de la vidéo.
Ex: https://vimeo.com/692252358'))
            ->conditional('banner_type', '==', 'vimeo');

        $page
            ->addFile('image_video_mobile', array('instructions' => 'Image à afficher en format mobile'))
            ->conditional('banner_type', '==', 'vimeo')
            ->or('banner_type', '==', 'youtube')
            ->or('banner_type', '==', 'video');

        acf_add_local_field_group($page->build());

        $condition = new FieldsBuilder('condition');


        $condition
            ->setLocation('page', '==', '355')
            ->or('page', '==', '2973');

        $condition
            ->addTab('circulation des voiturettes', [
                'placement' => 'left'
            ])
            ->addText('circulation')
            ->addTab('vitesse des verts', [
                'placement' => 'left'
            ])
            ->addText('parcours_royal', ['ui' => 1])
            ->addText('parcours_quebec', ['ui' => 1]);



        acf_add_local_field_group($condition->build());
    }

}

new page();
