<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class menu
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_filter('wp_nav_menu_objects', [$this, "menu_object"], 10, 2);
//        add_action('transition_post_status', [$this, 'notify'], 10, 3);

    }

    public function init()
    {
        $this->init_acf();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->menu_field()->build());
    }

    public function menu_field()
    {
        $menu = new FieldsBuilder('menu');

        $menu
            ->setLocation('nav_menu_item', '==', 'all');
        $menu
            ->addImage('menu_image');

        return $menu;
    }

    public function menu_object($items, $args)
    {
        foreach ($items as &$item) {

            $menu_image = get_field('menu_image', $item);

            if ($menu_image) {
                $item->title = sprintf(
                    '%1$s %2$s',
                    '<div class="sub-menu-img"><img width="420" height="280" class="h-100" src="' . wp_get_attachment_image_url($menu_image['id'], array(420, 280)) . '"></div>',
                    $item->title
                );
            }
        }

        return $items;
    }






}

new menu();


