<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class video
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Videos', 'video'),
            'singular_name' => __('Vidéo', 'video'),
            'menu_name' => __('Videos', 'video'),
            'all_items' => __('Tout les Videos', 'video'),
            'view_item' => __('Voir toute les Videos', 'video'),
            'add_new_item' => __('Ajouter une nouvelle Vidéo', 'video'),
            'add_new' => __('Ajouter', 'video'),
            'edit_item' => __('Éditer', 'video'),
            'update_item' => __('Modifier', 'video'),
            'search_items' => __('Rechercher', 'video'),
            'not_found' => __('Aucun résultat', 'video'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'video'),
        );

        $args = array(
            'label' => __('Videos', 'video'),
            'description' => __('Videos', 'video'),
            'labels' => $labels,
            'supports' => array('title', 'thumbnail'),
            'show_in_rest' => true,
            'hierarchical' => false,
            'publicly_queryable' => false,
            'public' => true,
            'has_archive' => false,
        );
        register_post_type('video', $args);

        register_taxonomy('video_category', ['video'], [
            'label' => __('Catégories de video', 'video'),
            'hierarchical' => false,
            'rewrite' => ['slug' => 'categories'],
            'show_admin_column' => true,
            'show_in_rest' => true
        ]);
        register_taxonomy_for_object_type('video_category', 'video');


        $this->init_acf();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->video_field()->build());
    }

    public function video_field()
    {
        $video = new FieldsBuilder('video');

        $video
            ->setLocation('post_type', '==', 'video');
        $video
            ->addText('video_author', ['label' => __('Invité spécial', 'royalquebec')])
            ->addText('video_id', ['label' => __('Youtube video ID', 'royalquebec')]);

        return $video;
    }
}


new video();


