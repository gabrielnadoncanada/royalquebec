<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class promotion
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_shortcode('promotions', [$this, 'promotions']);

    }

    public function init()
    {

        $labels = array(
            'name' => __('Partenaires', 'promotion'),
            'singular_name' => __('Partenaire', 'promotion'),
            'menu_name' => __('Partenaires', 'promotion'),
            'all_items' => __('Tout les Partenaires', 'promotion'),
            'view_item' => __('Voir tout les Partenaires', 'promotion'),
            'add_new_item' => __('Ajouter un nouveau Partenaire', 'promotion'),
            'add_new' => __('Ajouter', 'promotion'),
            'edit_item' => __('Éditer', 'promotion'),
            'update_item' => __('Modifier', 'promotion'),
            'search_items' => __('Rechercher', 'promotion'),
            'not_found' => __('Aucun résultat', 'promotion'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'promotion'),
        );

        $args = array(
            'label' => __('Partenaire', 'promotion'),
            'description' => __('Partenaires', 'promotion'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('partenaires', 'promotion')],
            'supports' => array('title', 'thumbnail'),
            'show_in_rest' => false,
            'publicly_queryable'  => false,
            'hierarchical' => false,
            'exclude_from_search' => true,
            'public' => true,
            'has_archive' => true,
        );
        register_post_type('promotion', $args);

        $this->init_acf();
    }

    public function promotion()
    {
        ob_start();
        $args = array(
            'post_type' => 'promotion',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);
        echo '<div class="teachers_link">';
        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
            <a data-action="modal" class="btn btn-accent mx-1 mb-2" href="#teacher"
               data-title="<?= __('FORMULAIRE DE RÉSERVATION AVEC <br> ', 'royalquebec') . get_the_title()?>"
               data-id="<?= get_the_ID()?>"
               data-email="<?= get_field('courriel', get_the_ID())?>"
            >
                <?= get_the_title() ?>
            </a>
        <?php
        endwhile;
        echo '</div>';

        $box_title = __('FORMULAIRE DE RÉSERVATION AVEC <br> ', 'royalquebec');

        $shortcode_content = '[modal listener_id="#teacher" box_title="' . $box_title . '"]';

        $shortcode_content .= '[gravityform id="1" title="false" description="false" ajax="true"]';
        $shortcode_content .= '[/modal]';
        echo do_shortcode($shortcode_content);

        wp_reset_postdata();
        return ob_get_clean();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->promotion_field()->build());
    }

    public function promotion_field()
    {
        $promotion = new FieldsBuilder('promotion');

        $promotion
            ->setLocation('post_type', '==', 'promotion');

        $promotion
            ->addUrl('url', ['label' => 'URL']);

        return $promotion;
    }
}

new promotion();
