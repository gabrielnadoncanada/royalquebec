<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class partner
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
        add_shortcode('partners', [$this, 'partners']);

    }

    public function init()
    {

        $labels = array(
            'name' => __('Partenaires', 'partner'),
            'singular_name' => __('Partenaire', 'partner'),
            'menu_name' => __('Partenaires', 'partner'),
            'all_items' => __('Tout les Partenaires', 'partner'),
            'view_item' => __('Voir tout les Partenaires', 'partner'),
            'add_new_item' => __('Ajouter un nouveau Partenaire', 'partner'),
            'add_new' => __('Ajouter', 'partner'),
            'edit_item' => __('Éditer', 'partner'),
            'update_item' => __('Modifier', 'partner'),
            'search_items' => __('Rechercher', 'partner'),
            'not_found' => __('Aucun résultat', 'partner'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'partner'),
        );

        $args = array(
            'label' => __('Partenaire', 'partner'),
            'description' => __('Partenaires', 'partner'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('partenaires', 'partner')],
            'supports' => array('title', 'thumbnail'),
            'show_in_rest' => false,
            'publicly_queryable'  => false,
            'hierarchical' => false,
          'exclude_from_search' => true,
            'public' => true,
            'has_archive' => true,
        );
        register_post_type('partner', $args);

        $this->init_acf();
    }

    public function partners()
    {
        ob_start();
        $args = array(
            'post_type' => 'partner',
            'posts_per_page' => -1,
        );

        $the_query = new WP_Query($args);

        echo "<div class='container vc_row wpb_row vc_row-fluid d-flex jc-center partners'><div class=\"slick\">";
        ?>

        <?php

        while ($the_query->have_posts()) : $the_query->the_post();
            ?>
            <div class="slide">
                <a target="_blank" rel="noopener" href="<?= get_field('url'); ?>">
                    <?= wp_get_attachment_image(get_post_thumbnail_id(get_the_ID()), array('auto', 82)) ?>
                </a>

            </div>
        <?php

        endwhile;

        echo "</div>";

        wp_reset_postdata();
        return ob_get_clean();
    }

    public function init_acf()
    {
        acf_add_local_field_group($this->partner_field()->build());
    }

    public function partner_field()
    {
        $partner = new FieldsBuilder('partner');

        $partner
            ->setLocation('post_type', '==', 'partner');

        $partner
            ->addUrl('url', ['label' => 'URL']);

        return $partner;
    }
}

new partner();
