<?php

$query = new WP_Query(array(
    'post_type' => 'course',
    'post_status' => 'publish'
));


$posts = $query->get_posts();


$courses = array_combine( wp_list_pluck( $posts, 'post_title' ), wp_list_pluck( $posts, 'ID' ) );


vc_map(
    array(
        "name" => __("Courses", "royalquebec"),
        "base" => "course",
        "class" => "",
        "category" => __("Content", "royalquebec"),
        "params" => array(
            array(
                "heading" => __("Parcours", "royalquebec"),
                "type" => "dropdown",
                "param_name" => "id",
                "value" => $courses
            ),
        )
    )
);
