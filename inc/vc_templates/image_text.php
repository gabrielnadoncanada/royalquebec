<?php

vc_map(
    array(
        "name" => __("Image Text", "royalquebec"),
        "base" => "image_text",
        "class" => "",
        "category" => __("Content", "royalquebec"),
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __("Titre", "royalquebec"),
                "param_name" => "custom_title",
            ),
            array(
                "type" => "textarea_html",
                "heading" => __("Contenu", "royalquebec"),
                "param_name" => "content",

            ),
            array(
                "type" => "textarea",
                "heading" => __("Contenu2", "royalquebec"),
                "param_name" => "custom_content",

            ),
            array(
                "type" => "attach_image",
                "heading" => __("Image", "royalquebec"),
                "param_name" => "custom_image",

            ),
            array(
                "type" => "checkbox",
                "heading" => __("Image à droite", "royalquebec"),
                "param_name" => "custom_image_position",
                "value" => false,
            ),
            array(
                "type" => "vc_link",
                "heading" => __("Boutton", "royalquebec"),
                "param_name" => "custom_btn",
                "value" => false
            )
        )
    )
);
