<?php

vc_map(
    array(
        "name" => __("Modal", "royalquebec"),
        "base" => "modal",
        "class" => "",
        "category" => __("Content", "royalquebec"),
        'content_element'         => true,
        'show_settings_on_create' => true,
        "params" => array(
            array(
                'type'        => 'textfield',
                'param_name'  => 'listener_id',
                'heading'     => esc_html__( 'Listener ID', 'element-plus' ),
                'description' => esc_html__( 'Add any hash-id as click listener to popup this modal box. Example: #open-modal', 'element-plus' ),
            ),
            array(
                'type'       => 'textfield',
                'param_name' => 'box_title',
                'heading'    => esc_html__( 'Box Title', 'element-plus' ),
            ),
            array(
                "type" => "textarea_html",
                "class" => "",
                "heading" => __( "Modal description", "redo" ),
                "param_name" => "content"
            )
        )
    )
);
