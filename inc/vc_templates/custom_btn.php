<?php


vc_map(
    array(
        "name" => __("Button", "royalquebec"),
        "base" => "custom_btn",
        "class" => "",
        "category" => __("Content", "royalquebec"),
        "params" => array(
            array(
                "type" => "vc_link",
                "heading" => __("Boutton", "royalquebec"),
                "param_name" => "btn_link"
            ),

            array(
                "type" => "dropdown",
                "heading" => __("Forme", "royalquebec"),
                "param_name" => "btn_shape",
                "value" => [
                    'Squared' => 'squared',
                    'Rounded' => 'rounded'
                ]
            ),

            array(
                "type" => "dropdown",
                "heading" => __("Couleur", "royalquebec"),
                "param_name" => "btn_color",
                "value" => [
                    'Primary' => 'primary',
                    'Accent' => 'accent',
                    'Transparent' => 'transparent'
                ]
            ),

            array(
                "type" => "dropdown",
                "heading" => __("Alignement", "royalquebec"),
                "param_name" => "btn_align",
                "value" => [
                    'Normal' => 'normal',
                    'Left' => 'left',
                    'Center' => 'center',
                    'Right' => 'right'
                ]
            ),
            array(
                "type" => "textfield",
                "heading" => __("ID", "royalquebec"),
                "param_name" => "extra_id"
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra class", "royalquebec"),
                "param_name" => "extra_class"
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra Attr", "royalquebec"),
                "param_name" => "extra_attr"
            )
        )
    )
);

