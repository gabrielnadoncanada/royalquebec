<?php

vc_map(
    array(
        "name" => __("Image Text Overlay", "royalquebec"),
        "base" => "image_text_overlay",
        "class" => "",
        "category" => __("Content", "royalquebec"),
        "params" => array(
            array(
                "type" => "textfield",
                "heading" => __("Titre", "royalquebec"),
                "param_name" => "custom_title"
            ),
            array(
                "type" => "textfield",
                "heading" => __("Sous titre", "royalquebec"),
                "param_name" => "custom_subtitle"
            ),
            array(
                "type" => "attach_image",
                "heading" => __("Image", "royalquebec"),
                "param_name" => "custom_image"
            )
        )
    )
);
