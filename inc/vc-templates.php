<?php


class _templates
{

    public function __construct()
    {
        add_action("vc_before_init", [$this, "get_templates"]);
    }

    /**
     * Return custom modules ex: partenaire, clients, membres
     */
    function get_templates()
    {
        foreach (glob(get_template_directory() . '/inc/vc_templates/*.php') as $key => $template) {
            if(is_file($template)){
                require_once($template);
            }
        }
    }

}

new _templates();


