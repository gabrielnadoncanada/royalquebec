<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package royalquebec
 */

if (!function_exists('royalquebec_posted_on')) :
    /**
     * Prints HTML with meta information for the current post-date/time.
     */
    function royalquebec_posted_on()
    {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if (get_the_time('U') !== get_the_modified_time('U')) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
        }

        $time_string = sprintf(
            $time_string,
            esc_attr(get_the_date(DATE_W3C)),
            esc_html(get_the_date()),
            esc_attr(get_the_modified_date(DATE_W3C)),
            esc_html(get_the_modified_date())
        );

        $posted_on = sprintf(
        /* translators: %s: post date. */
            esc_html_x('Posted on %s', 'post date', 'royalquebec'),
            '<a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . $time_string . '</a>'
        );

        echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

    }
endif;

if (!function_exists('royalquebec_posted_by')) :
    /**
     * Prints HTML with meta information for the current author.
     */
    function royalquebec_posted_by()
    {
        $byline = sprintf(
        /* translators: %s: post author. */
            esc_html_x('by %s', 'post author', 'royalquebec'),
            '<span class="author vcard"><a class="url fn n" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a></span>'
        );

        echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

    }
endif;

if (!function_exists('royalquebec_entry_footer')) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function royalquebec_entry_footer()
    {
        // Hide category and tag text for pages.
        if ('post' === get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(esc_html__(', ', 'royalquebec'));
            if ($categories_list) {
                /* translators: 1: list of categories. */
                printf('<span class="cat-links">' . esc_html__('Posted in %1$s', 'royalquebec') . '</span>', $categories_list); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', esc_html_x(', ', 'list item separator', 'royalquebec'));
            if ($tags_list) {
                /* translators: 1: list of tags. */
                printf('<span class="tags-links">' . esc_html__('Tagged %1$s', 'royalquebec') . '</span>', $tags_list); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
            }
        }

    }
endif;

if (!function_exists('royalquebec_post_thumbnail')) :
    /**
     * Displays an optional post thumbnail.
     *
     * Wraps the post thumbnail in an anchor element on index views, or a div
     * element when on single views.
     */
    function royalquebec_post_thumbnail()
    {
        if (post_password_required() || is_attachment() || !has_post_thumbnail()) {
            return;
        }

        if (is_singular()) :
            ?>

            <div class="post-thumbnail">
                <?php the_post_thumbnail(); ?>
            </div><!-- .post-thumbnail -->

        <?php else : ?>

            <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
                <?php
                the_post_thumbnail(
                    'post-thumbnail',
                    array(
                        'alt' => the_title_attribute(
                            array(
                                'echo' => false,
                            )
                        ),
                    )
                );
                ?>
            </a>

        <?php
        endif; // End is_singular().
    }
endif;

if (!function_exists('wp_body_open')) :
    /**
     * Shim for sites older than 5.2.
     *
     * @link https://core.trac.wordpress.org/ticket/12563
     */
    function wp_body_open()
    {
        do_action('wp_body_open');
    }
endif;

if (!function_exists('search_content_highlight')) :
    function search_content_highlight($content)
    {
        $content = strip_tags(apply_filters('the_content', $content));

        $content = trim(preg_replace('/\s+/', ' ', $content));

        $keys = get_query_var('s');

        $prev = '<span class="search-instance">';
        $after = '</span>';
        $content = preg_replace('/(' . $keys . ')/iu', $prev . '\0' . $after, $content);


        $minLength = 150; //Number of Characters you want to display minimum

        $start_trim = 0;
        $start = -1;
        $end = -1;

        $pos = strpos($content, $keys);
        if (!($pos === false)) {
            if ($start == -1 || $pos < $start)
                $start = $pos - (strlen($prev) + $start_trim); //To take into account the <strong class="search-instance">
            if ($end == -1 || $pos + strlen($keys) > $end)
                $end = $pos + strlen($keys) + strlen($after) + $start_trim; //To take into account the full string and the </strong>
        }


        if (strlen($content) < $minLength) {
            $start = 0;
            $end = strlen($content);
        }

        if ($start == -1 && $end == -1) {
            $start = 0;
            $end = $minLength;
        } else if ($start != -1 && $end == -1) {
            $start = ($start + $minLength <= strlen($content)) ? $start : strlen($content) - $minLength;
            $end = $start + $minLength;
        } else if ($start == -1 && $end != -1) {
            $end = ($end - $minLength >= 0) ? $end : $minLength;
            $start = $end - $minLength;
        }

        return "<p>" . (($start != 0) ? '...' : '') . substr($content, $start, $end - $start) . (($end != strlen($content)) ? '...' : '') . "</p>";
    }
endif;
