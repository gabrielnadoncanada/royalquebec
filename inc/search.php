<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

class custom_search
{
    public function __construct()
    {
        add_action('wp_footer', [$this, 'search_content_validate']);
    }

    public function search_content_highlight($content)
    {
        $content = strip_tags(apply_filters('the_content', $content));

        $content = trim(preg_replace('/\s+/', ' ', $content));

        $keys = get_query_var('s');

        $prev = '<span class="search-instance">';
        $after = '</span>';
        $content = preg_replace('/(' . $keys . ')/iu', $prev . '\0' . $after, $content);


        $minLength = 150; //Number of Characters you want to display minimum

        $start_trim = 0;
        $start = -1;
        $end = -1;

        $pos = strpos($content, $keys);
        if (!($pos === false)) {
            if ($start == -1 || $pos < $start)
                $start = $pos - (strlen($prev) + $start_trim); //To take into account the <strong class="search-instance">
            if ($end == -1 || $pos + strlen($keys) > $end)
                $end = $pos + strlen($keys) + strlen($after) + $start_trim; //To take into account the full string and the </strong>
        }


        if (strlen($content) < $minLength) {
            $start = 0;
            $end = strlen($content);
        }

        if ($start == -1 && $end == -1) {
            $start = 0;
            $end = $minLength;
        } else if ($start != -1 && $end == -1) {
            $start = ($start + $minLength <= strlen($content)) ? $start : strlen($content) - $minLength;
            $end = $start + $minLength;
        } else if ($start == -1 && $end != -1) {
            $end = ($end - $minLength >= 0) ? $end : $minLength;
            $start = $end - $minLength;
        }

        return "<p>" . (($start != 0) ? '...' : '') . substr($content, $start, $end - $start) . (($end != strlen($content)) ? '...' : '') . "</p>";
    }


    public function search_content_validate()
    {
        ?>
        <script>
            let validated = false;

            jQuery(function(){
                jQuery('.search-form').submit(function(event) {
                    let val = jQuery(this).find('.search-field').val().replace(/\s/g, "");
                    if(val.length > 3){
                        return true;
                    } else {
                        if(jQuery(this).find('.search-error').length <= 0){
                            let validation_message = '<?= __('Veuillez entrer plus de 3 caractères', 'royalquebec'); ?>';

                            jQuery(this).append('<span class="search-error">'+validation_message+'</span>');
                        }
                        return false;
                    }
                });
            });
        </script>

        <?php
    }
}

new custom_search();
